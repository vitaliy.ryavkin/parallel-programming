#include <iostream>
#include <omp.h>
#include <Windows.h>

int main()
{
    int k;
    std::cin >> k;
    int rank = -1;
#pragma omp parallel num_threads(k) private(rank)
    {
        rank = omp_get_thread_num();
        Sleep(1000);
        printf("I am %d thread.\n", rank);
    }
    
    system("pause");
}