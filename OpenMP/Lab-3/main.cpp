#include <iostream>
#include <omp.h>

int main()
{
    int k;
    std::cin >> k;

    omp_set_num_threads(k);
#pragma omp parallel
    {
        printf("I am %d thread from %d threads\n", omp_get_thread_num(), omp_get_num_threads());
    }

    system("pause");
}