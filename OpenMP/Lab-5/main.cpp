#include <iostream>
#include <omp.h>
#include <Windows.h>

int main()
{
    int n;
    std::cin >> n;

    int k;
    std::cin >> k;

    int sum = 0;
#pragma omp parallel for reduction(+ \
                                   : sum) num_threads(k)
    for (int i = 0; i < n; i++)
    {
        sum += i;
        printf("[%d]: Sum = %d\n", omp_get_thread_num(), sum);
    }

    std::cout << sum;

    system("pause");
}